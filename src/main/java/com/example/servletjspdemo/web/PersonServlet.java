package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.service.StorageService;

/**
 * Servlet implementation class SimpleServlet
 */
@WebServlet("/PersonServlet")
public class PersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		
		StorageService db = null;
		
		if(session.getAttribute("db") != null)
			db = (StorageService) session.getAttribute("db");
		else
			db = new StorageService();
		
		Person person = new Person();
		person.setFirstName(request.getParameter("name"));
		person.setYob(Integer.parseInt(request.getParameter("yearOfBirth")));
		
		db.add(person);
		
		session.setAttribute("db", db);
		
		out.println("Lista osob:");
		for (Person p : db.getAllPersons()) {
			out.println(p.getFirstName() + " " + p.getYob());
		}
		
	}
}
