package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SimpleServlet
 */
@WebServlet("/SimpleServlet")
public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("name");
		
		HttpSession session = request.getSession();
		
		ServletContext context = request.getServletContext();
		
		if(name != null && name != "null") {
			session.setAttribute("name", name);
			context.setAttribute("name", name);
		}
		
		out.println("Hello World " + name);
		out.println("Zmienna sesyjna (name): " + session.getAttribute("name"));
		out.println("Zmienna kontekstowa (name): " + context.getAttribute("name"));
	}
}
